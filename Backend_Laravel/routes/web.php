<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::post('status-change/:id', [HomeController::class, 'statusChange'])->name('status-change');

Route::resource('users', HomeController::class);
// Route for get user for yajra post request.
Route::get('get-users', [HomeController::class, 'getUsers'])->name('get-users');

