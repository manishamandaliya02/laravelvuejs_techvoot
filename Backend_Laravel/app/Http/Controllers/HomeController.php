<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\City;
use App\Models\UserHobby;
use DataTables;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Get the data for listing in yajra.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getUsers(Request $request, User $user)
    {
        $data = $user->getData();
        return \DataTables::of($data)
            ->addColumn('Actions', function($data) {
                return '<button type="button" class="btn btn-success btn-sm" id="getEditUserData" data-id="'.$data->id.'">Edit</button>
                    <button type="button" class="btn btn-success btn-sm" id="changeStatusData" data-id="'.$data->id.'">'.$data->status.'</button>
                    <button type="button" data-id="'.$data->id.'" data-toggle="modal" data-target="#DeleteUserModal" class="btn btn-danger btn-sm" id="getDeleteId">Delete</button>';
            })
            ->rawColumns(['Actions'])
            ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $city = City::all();
        $user = new User;
        $data = $user->findData($id);
        $html = '<div class="form-group">
                    <label for="FirstName">FirstName:</label>
                    <input type="text" class="form-control" name="firstname" id="editFirstName" value="'.$data->firstname.'">
                </div>
                <div class="form-group">
                    <label for="LastName">LastName:</label>
                    <input type="text" class="form-control" name="lastname" id="editLastName" value="'.$data->lastname.'">
                </div>
                <div class="form-group">
                    <label for="City">City:</label>
                    <select class="form-control">';
                     foreach($city as $key=>$value){
                        $html .='<option>'.$value['name'].'</option>';
                     }
                        
                     $html .='</select>
                </div>';

        return response()->json(['html'=>$html]);
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = new User;
        $user->deleteData($id);

        return response()->json(['success'=>'User deleted successfully']);
    }

     /**
     * Change status the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $user = new User;
        $user->updateData($id, $request->all());

        return response()->json(['success'=>'User deleted successfully']);
    }
}
