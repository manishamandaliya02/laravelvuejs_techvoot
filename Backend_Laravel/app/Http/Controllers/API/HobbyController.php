<?php
   
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Hobby;
   
class HobbyController extends BaseController
{
    /**
     * Hobby api
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllHobby()
    {
        $hobby = Hobby::all(); 
           
        return $this->sendResponse($hobby, 'successfully.');
        
    }
}