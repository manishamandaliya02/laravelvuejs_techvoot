<?php
   
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\City;
   
class CityController extends BaseController
{
    /**
     * City api
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllCity()
    {
        $city = City::all(); 
           
        return $this->sendResponse($city, 'successfully.');
        
    }
}