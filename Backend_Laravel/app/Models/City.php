<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class City extends Model
{
    protected $table = 'cities';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

}
