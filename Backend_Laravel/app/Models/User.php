<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\Models\City;
use App\Models\UserHobby;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id',
        'firstname',
        'lastname',
        'email',
        'password',
        'city_id',
        'gender',
        'status',
        'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id','id');
    }

    public function user_hobbies()
    {
        return $this->hasMany(UserHobby::class,'user_id','id');
    }

    public function findData($id)
    {
        return static::with('city')->find($id);
    }

    public function getData()
    {
        return static::orderBy('created_at','desc')->Where('role_id','2')->get();
    }

    public function deleteData($id)
    {
        return static::find($id)->delete();
    }

    public function updateData($id, $input)
    {
        return static::find($id)->update($input);
    }
}
