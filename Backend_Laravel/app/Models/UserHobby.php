<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Hobby;

class UserHobby extends Model
{
    protected $table = 'user_hobbies';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'hobby_id',
    ];

    public function hobbies()
    {
        return $this->hasMany(Hobby::class, 'id'); 
    }

    

}
