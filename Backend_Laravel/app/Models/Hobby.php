<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\UserHobby;

class Hobby extends Model
{
    protected $table = 'hobbies';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

}
