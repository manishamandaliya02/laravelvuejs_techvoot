<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Hobby;

class HobbySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Hobby::truncate();
        Hobby::insert([[
            'name' => 'Music',
        ],
        [
            'name' => 'Travelling',
        ],
        [
            'name' => 'Singing',
        ],
        [
            'name' => 'Cooking',
        ]]);
    }
}
