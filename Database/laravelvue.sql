-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 13, 2021 at 07:21 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravelvue`
--

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Ahmedabad', '2021-05-12 17:59:31', '2021-05-12 17:59:31'),
(2, 'Rajkot', '2021-05-12 17:59:31', '2021-05-12 17:59:31'),
(3, 'Mumbai', '2021-05-12 17:59:31', '2021-05-12 17:59:31'),
(4, 'Delhi', '2021-05-12 17:59:31', '2021-05-12 17:59:31');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Music', '2021-05-12 18:04:02', '2021-05-12 18:04:02'),
(2, 'Travelling', '2021-05-12 18:04:02', '2021-05-12 18:04:02'),
(3, 'Singing', '2021-05-12 18:04:02', '2021-05-12 18:04:02'),
(4, 'Cookies', '2021-05-12 18:04:02', '2021-05-12 18:04:02');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(10, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(11, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(12, '2016_06_01_000004_create_oauth_clients_table', 2),
(13, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('024810e681b33964878bcf88dcb9c19330dba86bf9f1faedfffa5d1442e159ac28305cf1c11e03bf', 9, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-12 21:07:03', '2021-05-12 21:07:03', '2022-05-12 14:07:03'),
('09fe6e20093cb662671feaed01cad382de1be2258fb6a46e48bf50ab3069558eb8adcb7494f5b658', 1, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-13 12:12:00', '2021-05-13 12:12:00', '2022-05-13 05:12:00'),
('0ea286bd94882d7e70ec1a01bdd01431dca9e1609b00cae29f8458ef89bab05640a89e1f888e653e', 9, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-12 20:42:57', '2021-05-12 20:42:57', '2022-05-12 13:42:57'),
('10266d9b1131f46d8ddcb72382aca6c723d2f2067483b20cd8001c697ab438e9bf4b5f9e67687108', 9, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-12 20:36:29', '2021-05-12 20:36:29', '2022-05-12 13:36:29'),
('2537c01349eb574a2c9d16a60fb666595ab7387d644ce6eb3961d1fd41d1bf094a1bb3e73a216f39', 9, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-12 20:35:25', '2021-05-12 20:35:25', '2022-05-12 13:35:25'),
('26ff97a50fb11264d364eefbcc381ee1538df9f15c7922b1fd0ed57d8b1c50b35a17c8d9f4dbeafe', 16, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'MyApp', '[]', 0, '2021-05-12 20:15:31', '2021-05-12 20:15:31', '2022-05-12 13:15:31'),
('2bffc41182a3bb0531ea9e4d25066891ad1c4e7066cffb2a2d1b2bd50d6997877ea20db970c4dd03', 9, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-12 21:11:26', '2021-05-12 21:11:26', '2022-05-12 14:11:26'),
('3595677a8bc006b14a56d4e7d8086b9522bcaa157cf44ec77d078289484b1c15340668bcdf4afed8', 21, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'MyApp', '[]', 0, '2021-05-12 20:26:13', '2021-05-12 20:26:13', '2022-05-12 13:26:13'),
('35ffc1cfcf1f0e00255a371fe39d329e56c8b668cf314e14a296375f8565613c8de706c6639dea3c', 36, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'MyApp', '[]', 0, '2021-05-13 01:25:42', '2021-05-13 01:25:42', '2022-05-12 18:25:42'),
('3a481e95b9148639a4994bbef3b461ef0baef6adf53ce7da28e9e4cef3c7d98f9d380194af9ffed9', 9, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-12 21:16:11', '2021-05-12 21:16:11', '2022-05-12 14:16:11'),
('3b52a6031e53edcfedf784870ee1ff3706578cfb005f91847bc50d8256e503cccb0c5ba12f7373c2', 9, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-12 21:23:25', '2021-05-12 21:23:25', '2022-05-12 14:23:25'),
('3bb0643f2519519297dca1443140713994da208a7f652c2b3f84bf8df3af970e0d1e11574372bb6f', 9, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-12 21:25:00', '2021-05-12 21:25:00', '2022-05-12 14:25:00'),
('3c1ac318c8095390a3c3be0b93b6248f8ff456dc3760ba0a2d69ce748b69cd9e1eaf63e06832e5ce', 22, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-12 21:23:45', '2021-05-12 21:23:45', '2022-05-12 14:23:45'),
('488416fd03cd8c1f64447f507384128b7160262c7bf4404fd18b413ffc1b423449ed3b0a7a52ce05', 9, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-12 21:06:08', '2021-05-12 21:06:08', '2022-05-12 14:06:08'),
('4a9b67d9354ebffe902f23c2ee6d18f675807c9500c58c84d5d607a9f142e2ee416b6a2fc11c1bc2', 9, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-12 20:45:35', '2021-05-12 20:45:35', '2022-05-12 13:45:35'),
('4bbc4c564be78e3c8e7506fcebe7735f745e75676226fded11ff6cdeb688d729bcc07330828e0f29', 1, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-13 12:11:56', '2021-05-13 12:11:56', '2022-05-13 05:11:56'),
('4f0f8fd4572f7a02415fe368103834fb8456b22ca5edc831b831c5d467581ecec98bcbf59a413312', 9, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-12 21:14:19', '2021-05-12 21:14:19', '2022-05-12 14:14:19'),
('56f2a9b84d7d289b8707aef8b705ea30def879fd4ee806358af6ca9e1c9eee4668614edda52717d4', 47, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'MyApp', '[]', 0, '2021-05-13 01:54:27', '2021-05-13 01:54:27', '2022-05-12 18:54:27'),
('617aa94d566b56894e51ba9df5a4a7d98def70b14a8b71dc42a6d040fb79b18aa4e183ca8e6642e4', 20, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'MyApp', '[]', 0, '2021-05-12 20:24:18', '2021-05-12 20:24:18', '2022-05-12 13:24:18'),
('670e20e3172e3d245605da4e5f2a27dbc95ad68d314afa5d0c161c563a84da1abe1a52c2c4950905', 19, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'MyApp', '[]', 0, '2021-05-12 20:20:34', '2021-05-12 20:20:34', '2022-05-12 13:20:34'),
('6a541ad3b175a3db8b1206752b72695ebe37e71b333d41dd93fd73940d9554188b95d28211e791f7', 9, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-12 21:05:21', '2021-05-12 21:05:21', '2022-05-12 14:05:21'),
('6c818e8846d5b68e1015002b1e96e5ec2cd6852f312206dce864ad4259748ea9f29401e968311b47', 9, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-12 21:23:23', '2021-05-12 21:23:23', '2022-05-12 14:23:23'),
('7443e055e2de7f400ecc71a41e30eecb6e0a7fd3f38801522bf307e5e6b41f43aec84031a78fe7cb', 9, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-12 21:15:26', '2021-05-12 21:15:26', '2022-05-12 14:15:26'),
('8a83ffc8b2b6ed6377fb9a0f6806ed9add0262f0f842cf04af20a4ed1c852d1a29ac0a071fd1035e', 9, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-12 20:46:51', '2021-05-12 20:46:51', '2022-05-12 13:46:51'),
('98da830a8ebf6a6796c367500668e0f044705045a92804989906652d5fcb0634def693081cf96b89', 9, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-12 21:08:56', '2021-05-12 21:08:56', '2022-05-12 14:08:56'),
('99e44e8ee3617743ab440ee5e1093c88fa1fdc2a8f47c99103a3095b5e317a5210367c3be4059627', 30, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'MyApp', '[]', 0, '2021-05-13 01:16:51', '2021-05-13 01:16:51', '2022-05-12 18:16:51'),
('9ca58bbb1c6a939af2e599762b2930e29da728481cf165e58ff5c2e46030b25f11cce6588b4cbb0b', 22, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'MyApp', '[]', 0, '2021-05-12 20:27:33', '2021-05-12 20:27:33', '2022-05-12 13:27:33'),
('a1f9e6c45fbc4a94849d543a98de02b575bd037c4e9bfc09f9af77fbef045affb6c319fe530b3256', 15, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'MyApp', '[]', 0, '2021-05-12 20:14:36', '2021-05-12 20:14:36', '2022-05-12 13:14:36'),
('a72a8ce48913d0d00c898c96f21ff5361f0741f9e208bc7096afe2c67b1c68c56d682fbd259f3e2d', 9, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-12 21:14:40', '2021-05-12 21:14:40', '2022-05-12 14:14:40'),
('a7370394d51b32169e7a799418703abfe8dddd8c3cbcbb4f20421df540db9963d9ceb0ac05840959', 23, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'MyApp', '[]', 0, '2021-05-13 01:11:28', '2021-05-13 01:11:28', '2022-05-12 18:11:28'),
('a7425a17dd9a8541568f6e8564fdefe1faacd600993d21625271753673d45060f40b492f64f7e1a4', 1, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-13 12:12:00', '2021-05-13 12:12:00', '2022-05-13 05:12:00'),
('a9006b2cee07a15893f779f6036ea72988796f111f20d850447aa0421d102285b90ffec0b8fc893d', 1, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-13 12:18:37', '2021-05-13 12:18:37', '2022-05-13 05:18:37'),
('a9e0e2e89edc429c524f93b70cee6317d4834ca6519cc4bfb9b0925dfa40b06a4487cd177b32a4f9', 37, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'MyApp', '[]', 0, '2021-05-13 01:26:30', '2021-05-13 01:26:30', '2022-05-12 18:26:30'),
('aa7b4790ccd37de75a4f43a55f0e3c6b72fb43a501432de7cf2abe7e0e68529fa6f8c659a2addd88', 33, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'MyApp', '[]', 0, '2021-05-13 01:22:06', '2021-05-13 01:22:06', '2022-05-12 18:22:06'),
('ad8d826b4afa22997655254596249715fed4e1a5e8e6cc8f8968096be9c95648411c1f432cd777b2', 9, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-12 21:11:03', '2021-05-12 21:11:03', '2022-05-12 14:11:03'),
('adae03a24dd152e1aa9744a5f6033066721d4039cb28a9c16cad5b706eb31427f9aeb9d7d36d6f4c', 1, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-13 12:11:59', '2021-05-13 12:11:59', '2022-05-13 05:11:59'),
('b901524f17aad1af2132cfbd5fd5e7e503270bffddd3835f1b4359f7a720e325502fdfcac87aa392', 17, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'MyApp', '[]', 0, '2021-05-12 20:16:05', '2021-05-12 20:16:05', '2022-05-12 13:16:05'),
('c6e37f21c0c655a93b878e42152df505a45b107db1ca81dddd247eda62dde0b255992ffba6616251', 9, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-12 21:04:57', '2021-05-12 21:04:57', '2022-05-12 14:04:57'),
('cb129e65fa00f3deb3f5f199d8004f71616cd09a0c24de3e204621825b7c55d73110217477092045', 14, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'MyApp', '[]', 0, '2021-05-12 20:13:28', '2021-05-12 20:13:28', '2022-05-12 13:13:28'),
('d0c98822e3406297d68234efcb68b2719f4b20a7f757fe98f0a3ae5d1a9e8f84d2d34300c6910bcb', 31, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'MyApp', '[]', 0, '2021-05-13 01:17:48', '2021-05-13 01:17:48', '2022-05-12 18:17:48'),
('d2186c60209cdf514369acb6509ecd3e931d837d137994b9e3518c26a414c43ac3f1ca33806c0058', 38, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'MyApp', '[]', 0, '2021-05-13 01:27:46', '2021-05-13 01:27:46', '2022-05-12 18:27:46'),
('d315fdb258796738f77d4534632946936d2c33ab8d93a54c9bbbbe1a1d66a737086f41f41559a2df', 9, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-12 20:40:20', '2021-05-12 20:40:20', '2022-05-12 13:40:20'),
('d3875acc2c0513cded1d6745cc8151e61e4f2b56edd14884c97b057b2a34c19408c48914e85d23a3', 9, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-12 20:56:07', '2021-05-12 20:56:07', '2022-05-12 13:56:07'),
('d665687525bb2882bd72afd44483eb8cdcb76f05caec626fabd3fa1a19212743f2f890b81df50368', 1, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-13 12:18:23', '2021-05-13 12:18:23', '2022-05-13 05:18:23'),
('d7b791e3fb0921c3ac26cb7e8a85a2e1bdbac29a324e45d959acfbe3a78bc7faf717c13aa751f075', 9, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'MyApp', '[]', 0, '2021-05-12 20:09:33', '2021-05-12 20:09:33', '2022-05-12 13:09:33'),
('ea746791498b25ee6a55afc655ea9090cbc9e8c471181e59064e4eaed7093d5293721c5569ae8910', 35, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'MyApp', '[]', 0, '2021-05-13 01:25:17', '2021-05-13 01:25:17', '2022-05-12 18:25:17'),
('eb9f94137566c83a34b0b8442122dea7037fcd3b4e8477a03b64d110223ecef52bb757d42f94486e', 32, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'MyApp', '[]', 0, '2021-05-13 01:20:20', '2021-05-13 01:20:20', '2022-05-12 18:20:20'),
('ee295917e91d30fe3df38fdb8c37a98fbc2a08457374d27948b8cf2f97b5b6f1f2ebe83013d9e0fe', 9, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-12 21:08:01', '2021-05-12 21:08:01', '2022-05-12 14:08:01'),
('f251d810205048284e1c362b2c77238311f6a4e9ea9a0d9ddba21fb3318891004981b067576adc03', 1, '936987e7-fb7e-40f4-843f-dde67c92a30a', 'authToken', '[]', 0, '2021-05-13 12:14:26', '2021-05-13 12:14:26', '2022-05-13 05:14:26');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
('936987e7-fb7e-40f4-843f-dde67c92a30a', NULL, 'Laravel Personal Access Client', 'YGKrkk2vXf0gKgURPLqhXp0zSYPRWzDmKuyIOuqu', NULL, 'http://localhost', 1, 0, 0, '2021-05-12 17:16:50', '2021-05-12 17:16:50'),
('936987e8-ced1-4b49-804e-85cc8bd4bb24', NULL, 'Laravel Password Grant Client', 'Icg7RWYlohMcPnsWPSrl7WA6GW4B4QKWEMOkBdvJ', 'users', 'http://localhost', 0, 1, 0, '2021-05-12 17:16:50', '2021-05-12 17:16:50');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, '936987e7-fb7e-40f4-843f-dde67c92a30a', '2021-05-12 17:16:50', '2021-05-12 17:16:50');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('approved','unapproved') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unapproved',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `firstname`, `lastname`, `email`, `password`, `city_id`, `gender`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'Admin', 'admin@gmail.com', '$2y$10$iGccZhPByd3R.ZsZrLfflODbuOVjj/8sxSrxcJJttU5CHo25RGX46', NULL, NULL, 'approved', NULL, '2021-05-13 11:55:41', '2021-05-13 11:55:41');

-- --------------------------------------------------------

--
-- Table structure for table `user_hobbies`
--

CREATE TABLE `user_hobbies` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `hobby_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role_name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2021-05-12 10:50:00', '2021-05-12 10:50:00'),
(2, 'user', '2021-05-12 10:50:00', '2021-05-12 10:50:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_hobbies`
--
ALTER TABLE `user_hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_hobbies`
--
ALTER TABLE `user_hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
