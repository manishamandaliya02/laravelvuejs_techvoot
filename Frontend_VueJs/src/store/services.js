import axios from "axios";

export default {
  namespaced: true,

  state: {
    userData: null
  },

  getters: {
    user: state => state.userData
  },

  mutations: {
    setAllCity(state, user) {
      state.allCity = user;
    }
  },

  actions: {
    getAllCity() {
      return axios.get(process.env.VUE_APP_API_URL + "getAllCity");
    },

    getAllHobby() {
      return axios.get(process.env.VUE_APP_API_URL + "getAllHobby");
    },
  }
};
