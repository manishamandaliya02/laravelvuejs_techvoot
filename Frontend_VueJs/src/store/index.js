import Vue from "vue";
import Vuex from "vuex";
import auth from "./auth";
import services from "./services";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    errors: []
  },

  getters: {
    errors: state => state.errors
  },

  mutations: {
    setErrors(state, errors) {
      console.log('errors',errors)
      state.errors = errors;
    }
  },

  actions: {},

  modules: {
    auth,
    services
  }
});
